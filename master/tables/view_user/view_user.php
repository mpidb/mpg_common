<?php

class tables_view_user { 

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

}
?>
