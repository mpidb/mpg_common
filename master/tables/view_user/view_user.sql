-- mpi-dcts own version aus NEW user-db
CREATE OR REPLACE VIEW view_user AS
 SELECT
  userID,
  login,
  CONCAT(nachname, ', ', vorname, ' (', IFNULL(login,'--'), ')') AS sort
 FROM
  mpidb_all_user.main_benutzer
 WHERE
  enabled = 1 AND CURDATE() <= endDate
 ORDER BY
  sort
;

-- mpg-version ohne externe DB (licman,inv,gfk,chem,user) - Auslieferzustand
-- ggf. benoetigte User fuer Auswahl hier eintragen oder in mpi_user\fields.ini Feld widget:type mit ';' deaktvieren
CREATE OR REPLACE VIEW view_user AS
 SELECT 
  '000001' AS userID, 'mpg_local' AS login, 'MPG, version (mpg_local)' AS sort
;

-- mpg-version aus user-db
CREATE OR REPLACE VIEW view_user AS
 SELECT
  employee_id AS userID,
  localID AS login,
  CONCAT(last_name, ', ', first_name, ' (', IFNULL(localID,'--'), ')') AS sort
 FROM
  mpidb_mpg_user.mpi_user
 WHERE
  active = 1
 ORDER BY
  login
;

