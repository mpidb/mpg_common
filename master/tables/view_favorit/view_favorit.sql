-- view auf list_reiter
CREATE OR REPLACE VIEW view_favorit AS
SELECT
 *
FROM
 list_reiter
WHERE
 favorit = '1'
ORDER BY reiter
;

-- db::list_reiter - add checkbox field 'favorit' & kateg not null
ALTER TABLE list_reiter CHANGE kategorie kategorie VARCHAR(30) NOT NULL;
ALTER TABLE list_reiter ADD favorit TINYINT(1) NOT NULL DEFAULT '0' AFTER reiter;
ALTER TABLE list_reiter CHANGE autoID autoID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
ALTER TABLE list_reiter MODIFY COLUMN kategorie VARCHAR(30) NOT NULL AFTER reiter;
ALTER TABLE list_reiter CHANGE bedeutung bedeutung VARCHAR(100) NULL;

 -- history erweiterung list_reiter und zeige neue Tabellen in list_reiter
ALTER TABLE list_reiter ADD history TINYINT(1) NOT NULL DEFAULT '0' AFTER favorit;
DROP TABLE IF EXISTS list_reiter__history;

