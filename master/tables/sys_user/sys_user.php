<?php

class tables_sys_user { 

  function getPermissions($record) {
    $user = Dataface_AuthenticationTool::getInstance()->getLoggedInUser();
    // first let's deal with the case that the user is not logged in.
    if ( !$user ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    $role = $user->val('role'); 
  //  if ( isAdmin($role) ) return Dataface_PermissionsTool::ALL();
    if ( $role == 'MANAGER' ) return Dataface_PermissionsTool::getRolePermissions('MANAGER');
    if ( $role == 'ADMIN' )   return Dataface_PermissionsTool::getRolePermissions('ADMIN');
 
    // Everybody else gets read only access to the table.
    // return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
    return Dataface_PermissionsTool::NO_ACCESS();
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( Dataface_Record $record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    $log = $record->strval('login');
    $sql = "SELECT login FROM view_user WHERE login = '$log'";
    list($res) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (isset($res)) return $table.'gruen';
    return $table;  // ohne color
  }

  function block__after_result_list_content() {
    echo 'Benutzer &nbsp;&nbsp';
    echo '<span style="background-color:#cfc;"> ist gültig </span>&nbsp;|&nbsp;';
    echo '<span> ist abgelaufen oder nicht aktiviert. </span>&nbsp;';
  }

  function role__renderCell( Dataface_Record $record ) {
    $table  = 'list_role';
    $action = 'browse';
    $field  = 'role';
    $tabID  = $record->strval($field);
    $name   = $record->strval('role');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

 // email link in browse
  function email__htmlValue(Dataface_Record $record ) {
    $mail = $record->strval('email');
    return '<a href="mailto:'.$mail.'">'.$mail.'</a>';
  }

  // email link in list
  function email__renderCell(Dataface_Record $record ) {
    $mail = $record->strval('email');
    if (!$mail) return;
    $name = explode('@', $mail);
    return '<div style="text-align:left;"><a href="mailto:'.$mail.'">'.$name[0].'@mpi-...</a></div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen (wird vor insert und update ausgefuehrt)
  function beforeSave(Dataface_Record $record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    if ($record->strval('email') == NULL) {
      $app = Dataface_Application::getInstance();
      $dn  = $app->_conf['_own']['dn'];
      $usr = $record->strval('login');
      $record->setValue('email', $usr.'@'.$dn);
    }
  }

}
?>
