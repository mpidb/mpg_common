#!/bin/bash
#
# Change authorisation from old mpi_users to new sys_user
# schachi 2018-07-30
#
# Requirements:
#  - old mpi_users exists and new sys_user not yet
#  - parameter path show root of database (where conf.ini exists)

  help="Execute:\t'$0 <path-to-db>' OR direct in root folder of database"

  if [[ $# -eq 1 ]]; then
    if [[ $1 = "-h" || $1 = "--help" ]]; then
      echo -e " $help"
      exit 0
    else
      if [[ ! -d $1 ]]; then
        echo -e " ERROR:\tPath $1 is not a folder or not exists"
        exit 1
      fi
    fi
    cd $1
  fi
 
  conf="conf.ini"
  auth="sys_user"
  role="list_role"
  colo="custom.css"
  view="view_user"

  if [[ ! -w $conf ]]; then
    echo -e "ERROR:\t'$conf' not found or not writable. Not a root folder of database."
    echo -e " $help"
    exit 1
  fi

  db=$(filename $PWD)

  echo -en " Change new authorisation in database '$db' in $PWD ? (y/[n]) : "
  read ok
  if [[ -z $ok ]]; then ok="n"; fi
  if [[ $ok != "y" ]]; then exit 0; fi

  # step - check if view_user used as vocabulary
  echo -e " STEP[1]:\tCheck if old view '$view' used in program codes"
  if [[ $(grep -rv '^;' tables | grep view_user | grep localID) ]]; then
    echo -e "\tBEFORE:"
    echo "$(grep -rv '^;' tables | grep -B1 view_user | grep -B1 localID)"
    echo -e "\tWARNING:\tOld view '$view' use in program code. Please fix manuell as first like this 'SELECT login, sort FROM view_user'"
    echo -en " Ignore ? (y/[n]) : "
    read ok
    if [[ -z $ok ]]; then ok="n"; fi
    if [[ $ok != "y" ]]; then exit 0; fi
  else
    echo -e "\tINFO:\t\told view '$view' not use in program code"
  fi

  # step - conf.ini
  echo -e " STEP[2]:\tChange parameter in section [_auth] in '$conf'"
  echo -e "\tBEFORE1:\t$(egrep '^users_table' $conf)"
  echo -e "\tBEFORE2:\t$(egrep '^username_column' $conf)"
  sed -i 's/^users_table = "mpi_users"/users_table = "sys_user"/' $conf
  sed -i 's/^username_column = "username"/username_column = "login"/' $conf
  echo -e "\tAFTER1:\t\t$(egrep '^users_table' $conf)"
  echo -e "\tAFTER2:\t\t$(egrep '^username_column' $conf)"

  # step - link sys_user
  echo -e " STEP[3]:\tChange link in tables for auth"
  if [[ ! -L tables/mpi_users ]]; then
    echo -e "\tWARNING:\tOld auth link no more exists"
  else
    echo -e " BEFORE[$step]:"
    echo -e "\t$(ls -l tables/mpi_users)"
    echo -e "\t$(rm -v tables/mpi_users)"
  fi
  if [[ -L tables/sys_user ]]; then
    echo -e "\tWARNING:\tNew auth link already exists"
  else
    echo -e "\tAFTER:"
    echo -e "\t$(ln -s ../../master/tables/$auth tables/)"
    echo -e "\t$(ls -l tables/$auth)"
  fi

  # step - link list_role
  echo -e " STEP[4]:\tCreate link for $role in tables for roles"
  if [[ -L tables/$role ]]; then
    echo -e "\tWARNING:\tNew role link already exists"
  else
    echo -e "\t$(ln -s ../../master/tables/$role tables/)"
    echo -e "\t$(ls -l tables/$role)"
  fi

  # step - set color viol
  echo -e " STEP[5]:\tSet color viol for used roles in $colo"
  if [[ ! -w $colo ]]; then
    echo -e "WARNING:\t'$colo' not found or not writable"
  else
    if [[ ! $(grep 'viol' $colo | grep '#ccf') ]]; then
      echo "tr.listing.odd.viol   td.row-actions-cell," >> $colo
      echo "tr.listing.even.viol  td.row-actions-cell { background-color: #ccf; }" >> $colo
      echo -e "\tAFTER1:\t\t$(grep 'tr.listing.odd.viol' $colo)"
      echo -e "\tAFTER2:\t\t$(grep 'tr.listing.even.viol' $colo)"
    else
      echo -e "\tINFO:\t\tColor is already set"
    fi
  fi

  # step - create table sys_user and copy old entries
  echo -e " STEP[6]:\tExcecute '$auth.sql' in database 'mpidb_$db'"
  if [[ ! -r tables/$auth/$auth.sql ]]; then
    echo -e "ERROR:\t'tables/$auth/$auth.sql' not found or not readable."
  else
    mysql -p -u root mpidb_$db < tables/$auth/$auth.sql
    if [[ $? -eq 0 ]]; then
      echo -e "\tINFO:\tSuccessfully"
    else
      echo -e "\tERROR:\tMysql script has an error"
    fi
  fi


