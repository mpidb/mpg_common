<?php

class tables_mpi_users { 

  function getPermissions($record) {
    $user = Dataface_AuthenticationTool::getInstance()->getLoggedInUser();
    // first let's deal with the case that the user is not logged in.
    if ( !$user ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    $role = $user->val('role'); 
  //  if ( isAdmin($role) ) return Dataface_PermissionsTool::ALL();
    if ( $role == 'MANAGER' ) return Dataface_PermissionsTool::getRolePermissions('MANAGER');
    if ( $role == 'ADMIN' )   return Dataface_PermissionsTool::getRolePermissions('ADMIN');
 
    // Everybody else gets read only access to the table.
    // return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
    return Dataface_PermissionsTool::NO_ACCESS();
  }

  function email__renderCell( &$record ) {
    $mail = $record->strval('email');
    return '<a href="mailto:'.$mail.'">'.$mail.'</a>';
  }

  function email__htmlValue( &$record ) {
    $mail = $record->strval('email');
    return '<a href="mailto:'.$mail.'">'.$mail.'</a>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  function beforeSave(&$record) {
    if ($record->strval('email') == NULL) {
      $app = Dataface_Application::getInstance();
      $dn  = $app->_conf['_own']['dn'];
      $usr = $record->strval('username');
      $record->setValue('email', $usr.'@'.$dn);
    }
  }

}
?>
