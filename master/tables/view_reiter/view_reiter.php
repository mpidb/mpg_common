<?php

class tables_view_reiter { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

}

?>
