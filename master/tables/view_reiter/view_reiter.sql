-- view auf list_reiter, weil COLLATE nicht erlaubt in xataface
CREATE OR REPLACE VIEW view_reiter AS
SELECT
 CONVERT(table_name USING utf8) COLLATE utf8_unicode_ci AS reiter,
 table_type
FROM
 information_schema.tables
WHERE
   table_schema = (SELECT DATABASE() FROM DUAL) AND
 ( table_type = 'base table' OR table_type = 'view' )
;

