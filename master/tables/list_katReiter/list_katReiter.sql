--
-- Tabellenstruktur für Tabelle `list_katReiter`
--

CREATE TABLE IF NOT EXISTS `list_katReiter` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Daten für Tabelle `list_katReiter`
--

INSERT INTO `list_katReiter` (`autoID`, `kategorie`) VALUES
(1, 'Ablage'),
(2, 'Auswertung'),
(3, 'Autorisierung'),
(4, 'Haupttabelle'),
(5, 'History'),
(6, 'Liste'),
(7, 'Programmierung'),
(8, 'View'),
(9, 'Zuordnung');

