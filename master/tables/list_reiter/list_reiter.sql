DROP TABLE IF EXISTS `list_reiter`;
CREATE TABLE IF NOT EXISTS `list_reiter` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `favorit` tinyint(1) NOT NULL DEFAULT '0',
  `history` tinyint(1) NOT NULL DEFAULT '0',
  `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `reiter` (`reiter`),
  KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface' AUTO_INCREMENT=9 ;

INSERT INTO `list_reiter` (`autoID`, `reiter`, `kategorie`, `favorit`, `history`, `bedeutung`) VALUES
(000001, 'tab_tabelle1', 'Haupttabelle', 1, 1, 'Haupttabelle 1'),
(000002, 'tab_tabelle2', 'Haupttabelle', 1, 1, 'Haupttabelle 2'),
(000003, 'list_katReiter', 'Liste', 0, 0, 'Zugehoerigkeit DB-Tabellen'),
(000004, 'list_reiter', 'Liste', 0, 0, 'Sammelcontainer fuer Tabbutton "mehr .."'),
(000005, 'sys_user', 'Autorisierung', 1, 0, 'Autorisierung und Berechtigung Benutzer'),
(000006, 'view_user', 'View', 0, 0, 'Auswahl Benutzer aus DB user'),
(000007, 'view_favorit', 'Programmierung', 0, 0, 'Für den schnelleren Zugriff unter dem Menüpunkt ''Favorit'),
(000008, 'view_reiter', 'Programmierung', 0, 0, 'Hole alle Tabellen von Datenbank von mysql');

ALTER TABLE `list_reiter`
  ADD CONSTRAINT `list_reiter_kateg` FOREIGN KEY (`kategorie`) REFERENCES `list_katReiter` (`kategorie`) ON UPDATE CASCADE;


--
-- Erweiterung vorhandener Tabellen
--
 ALTER TABLE list_reiter MODIFY COLUMN bedeutung varchar (100) AFTER kategorie;
 ALTER TABLE list_reiter ADD favorit TINYINT(1) NOT NULL DEFAULT '0' AFTER kategorie;
 ALTER TABLE list_reiter ADD history TINYINT(1) NOT NULL DEFAULT '0' AFTER favorit;
 ALTER TABLE list_reiter CHANGE autoID tabID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
 DROP TABLE IF EXISTS list_reiter__history;
 CREATE OR REPLACE VIEW view_favorit AS SELECT * FROM list_reiter WHERE favorit = '1';

