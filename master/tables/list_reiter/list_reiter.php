<?php

class tables_list_reiter { 

  function block__after_result_list_content() {
    echo 'Historie-Flags in ';
    echo '<span> <font color="blue">Blau</font></span>&nbsp;|&nbsp;';
    echo '<span> <font color="red">Rot</font></span>&nbsp;';
    echo '<span> sind nicht veränderbar (gesetzt in conf.ini).</span>&nbsp;';
  }

/*
  // geht leider wegen xataface parser nicht, deshalb per view
  function __sql__() {
    $app = Dataface_Application::getInstance();
    $db  = $app->_conf['_database']['name'];
    return "SELECT lst.*, IF(shw.reiter IS NULL, 0, 1) AS exist FROM list_reiter AS lst LEFT JOIN ( SELECT CONVERT(table_name USING utf8) COLLATE utf8_unicode_ci AS reiter, table_type FROM information_schema.tables WHERE table_schema = '$db' AND ( table_type = 'base table' OR table_type = 'view' ))AS shw ON lst.reiter = shw.reiter";
  }
*/

  // loeschen nur fuer admin oder manager erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    $role = $user->val('role'); // get Role from sys_user
    if ( $role == 'MANAGER' or $role == 'ADMIN') return;
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // zeige alle noch nicht erfassten tabellen als auswahl
  function valuelist__tables() {
    $app = Dataface_Application::getInstance();
    $db  = $app->_conf['_database']['name'];
    static $user = -1;
    if ( !is_array($tables) ) {
      $tables = array();
      $res = xf_db_query("SHOW FULL TABLES FROM $db WHERE NOT EXISTS ( SELECT * FROM list_reiter AS lst WHERE lst.reiter = Tables_in_$db COLLATE utf8_unicode_ci )", df_db());
      if ( !$res ) throw new Exception(xf_db_error(df_db()));
      while ($row = xf_db_fetch_row($res)) $tables[$row[0]] = $row[1].' : '.$row[0];
    }
    return $tables;
  }

  function reiter__renderCell( &$record ) {
    $table = $record->strval('reiter');
    if ( $record->strval('exist') == '0' ) return '<font color="red">'.$table.'</font>';
    return '<a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'">'.$table.'</a>';
  }

  function valuelist__favorit() {
    return array(0=>'Nein', 1=>'Ja');
  }

  function valuelist__history() {
    return array(0=>'Nein', 1=>'Ja');
  }

  function history__rendercell( &$record ) {
    $app = Dataface_Application::getInstance();
    if ( isset($app->_conf['history']['enabled']) AND @$app->_conf['history']['enabled'] == 0 ) return '<font color="red">Nein</font>';
    if ( strpos($record->strval('reiter'), 'view_') !== false) return '<font color="red">Nein</font>';
    if ( isset($app->_conf['_history'][$record->strval('reiter')])) {
      if ( @$app->_conf['_history'][$record->strval('reiter')] == 0 ) return '<font color="blue">Nein</font>';
      if ( @$app->_conf['_history'][$record->strval('reiter')] == 1 ) return '<font color="blue">Ja</font>';
    } 
    if ( !isset($app->_conf['history']['enabled'])) return '<font color="red">Nein</font>';
    return $record->display('history');
  }

  function beforeSave(&$record) {
    if ($record->strval('bedeutung') == NULL) {
      $record->setValue('bedeutung', $record->strval('reiter'));
    }
    // history fuer table in conf.ini geschalten, dann setzte history auch, history_enable dominant
    $app = Dataface_Application::getInstance();
    if ( isset($app->_conf['_history'][$record->strval('reiter')])) {
      if ( @$app->_conf['_history'][$record->strval('reiter')] == 0 ) $record->setValue('history', 0);
      if ( @$app->_conf['_history'][$record->strval('reiter')] == 1 ) $record->setValue('history', 1);
    }
    if ((!isset($app->_conf['history']['enabled']) AND !isset($app->_conf['_history'][$record->strval('reiter')])) OR
       (isset($app->_conf['history']['enabled']) AND @$app->_conf['history']['enabled'] == 0 ))
       $record->setValue('history', 0);
  }

}
?>
