<?php

class tables_list_role { 

  // loeschen nur fuer admin oder manager erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    $role = $user->val('role'); // get Role from sys_user
    if ( $role == 'MANAGER' or $role == 'ADMIN') return;
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function getTitle( Dataface_Record $record ) {
    return  $record->strval('role');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( Dataface_Record $record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    $usr = $record->getRelatedRecordObjects('user');
    if ($usr) return $table.'viol';
    return $table;  // ohne color
  }

  function block__after_result_list_content() {
    echo 'Rolle &nbsp;&nbsp';
    echo '<span style="background-color:#ccf;"> wird verwendet </span>&nbsp;';
  }

}
?>
