-- new table list_role instead roles from valuelists.ini

-- create list_role
CREATE TABLE IF NOT EXISTS list_role (
  `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- add roles in list_role
INSERT IGNORE INTO `list_role` (`rolID`, `role`, `description`) VALUES
(000001, 'NO ACCESS', 'No_Access'),
(000002, 'READ ONLY', 'view, list, calendar, view xml, show all, find, navigate'),
(000003, 'EDIT', 'READ_ONLY and edit, new record, remove, import, translate, copy'),
(000004, 'DELETE', 'EDIT and delete and delete found'),
(000005, 'OWNER', 'DELETE except navigate, new, and delete found'),
(000006, 'REVIEWER', 'READ_ONLY and edit and translate'),
(000007, 'USER', 'READ_ONLY and add new related record'),
(000008, 'ADMIN', 'DELETE and xml_view'),
(000009, 'MANAGER', 'ADMIN and manage, manage_migrate, manage_build_index, and install'),
(000010, 'GROUP MANAGER', '(Extended USER) and EDIT in Table User & Role');

-- add entry in list_reiter
INSERT INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('list_role', 'Authorisierung', 1, 0, 'Liste aller Berechtigungen (Rollen)');
