<?php

/*
 * Xataface Web Application Framework
 * Copyright (C) 2005-2008 Web Lite Solutions Corp (shannah@sfu.ca)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *-------------------------------------------------------------------------------
 */

/******************************************************************************
 * File:        <database>/actions/printable_report.php     
 * Author:      schachi@mpi-magdeburg.mpg.de
 * Created:     2015-01-29
 * Description: Html formatted print report for all tables
 *      
 *****************************************************************************/

/******************************************************************************
 * modified custom <database>/actions.ini
 *
 * ...
 * [printable_report]
 * icon="{$site_url}/images/printer.png"
 * category=result_list_actions
 * description="See this product list in a printable format"
 * url="{$app->url('-action=printable_report')}"
 * ...
 *
 *****************************************************************************/

class actions_printable_report {
  function handle(&$params) {
    //import('Dataface/RecordReader.php');
    $app =& Dataface_Application::getInstance();
    $query = $app->getQuery();
    $query['-skip'] = 0;
    $limit = 999;
    $query['-limit'] = $limit;
    $table = Dataface_Table::loadTable($query['-table']);
//    $records = df_get_records_array($query['-table'], $query);
$records = df_get_records_array($query['-table'], $query,null,null,true);

    echo '<html>'
         .'<head>'
          .'<title>Printable Report</title>'
          .'<link rel="stylesheet" type="text/css" href="../xataface/plone.css">'
          .'<script language="javascript">function printpage() { window.print(); }</script>'
          .'<script type="text/javascript" src="../xataface/plone_javascripts.js"></script>'
         .'</head>'
         .'<body onload="printpage()">'
          ."Maximum number of rows is $limit"
          .'<table id="print_list" class="listing">'
           .'<thead>'
            .'<tr class="table-headings">'
             .'<th class="row-actions-header"></th>';
    $header = array_keys($table->fields(false, true));
    foreach ($header as $colhead) {
      echo    "<th class='sorted-column-asc coltype-varchar'>$colhead</th>";
    }
      echo   '</tr>'
           .'</thead>'
           .'<tfoot style="display:none"></tfoot>'
           .'<tbody>';
    $list = 'odd';
    foreach ($records as $record) {
      $columns = array_keys($record->_table->fields(false, true));
      echo   "<tr class='listing $list'>"
             .'<td class="row-actions-call"></td>';
      foreach ($columns as $key) {
        $value = utf8_decode($record->display($key));
        echo  "<td class='field-content resultListCell varchar odd'><span>$value</span></td>";
      }
      echo   '</tr>';
      if ($list == 'odd') $list = 'even'; else $list = 'odd';
    }
    echo    '</tbody>'
          .'</table>'
         .'</body>'
        .'</html>';
  }
}

?>
