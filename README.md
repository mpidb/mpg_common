## Beschreibung ##

Dieses Projekt **common** kann in den DocumentRoot des Webbrowsers z.B /var/www/html oder auch in ein Subdirectory geladen werden, wenn es noch andere Webanwendungen geben sollte.<br>
Basis fuer alle Datenbanken ist das gemeinsame Hauptverzeichnis **Xataface** und einige von mir erstellte Ordner.<br>
Jede Projekt-Datenbank, die mit Xataface, arbeitet hat viele Gemeinsamkeiten, welche nicht immer wieder explizit erzeugt werden muessen/sollten.
Gemeinsamkeiten sind zum Beispiel:

Ordner   | Bedeutung
-------- | ---------
template | Vorlage fuer neue DB-Projekte
master   | gemeinsam nutzbare Programmierungen
xataface | link zur aktuellen Version
xataface-2.1.2 | aktuelle Version

Es erleichert erheblich die Arbeit mit verlinkten Files/Foldern zu arbeiten, weil nur noch einmal an einer Stelle programmieren muss. Dies muss man natuerlich beachten, wenn man Aenderungen macht. Denn diese wirken sofort auf alle anderen DB's.<br>

Im Ordner **db_export** sind noch einmal alle DB's und das Basisverzeichnis als tar abgelegt.<br>
Fuer den schnellen Erfolg bzw. fuer vorbelegte Rechte auf Dateiebene oder zum mergen ohne git gut geeignet.

## Installation ##

Lade diese Projekt in dein DocumentRoot oder in ein Subdirectory des Webbrowsers (z.B. /var/www/html/).<br>
### per git ###
~~~bash
cd <DocumentRoot>
git clone https://gitlab.mpcdf.mpg.de/mpidb/mpg_common.git 
~~~
### per tar ###
Download von db_export die Datei mpidb_mpg_common.tar.gz und entpacke sie in dein Webfolder.
Das hat den Vorteil das alle Permissions schon richtig gesetzt sind.<br>
Hier liegen auch die tar's der Projekte. 

Mehr Infos siehe README in den jeweiligen Projektfolder <projekt>/install/LIESMICH.txt

## Sicherheit ##

Um das Ausfuehren von Code ueber den Webbrowser zu verhindern, muessen Abwehrmassnahmen eingestellt werden. Dafuer existieren im Ordner master die Dateien .htaccess und Web.config, welche in den Unterordnern dann verlinkt werden.

Inhalt .htaccess :
~~~apache
<FilesMatch "\.(ini|sql|php|pl)$">
  Deny from all
</FilesMatch>
<FilesMatch "^(index|info)\.php$">
  Allow from all
</FilesMatch>
~~~

Stelle sicher dass die .htaccess vom Webbrowser beachtet wird. Teste z.B. mit Eingabe der URL http[s]://\<db-server\>/\<projekt\>/conf.ini im Browser. Hier sollte FORBIDDEN als Meldung kommen. Wenn nicht ggf. den Apachen mit folgende Aenderungen im DocumentRoot in der 000-default.conf bzw. default-ssl.conf vornehmen.
~~~apache
# DocumentRoot
<Directory /var/www/html>
  Options Indexes FollowSymLinks MultiViews
  # aendern
  #AllowOverride none
  AllowOverride All
  Order allow,deny
  allow from all

  # hinzufuegen
  # Denied xataface execute ini, sql, php
  <FilesMatch ".+\.(ini|sql)">
    Order deny,allow
    Deny from All
  </Filesmatch>
</Directory>
~~~

## siehe auch ##
Init-Wiki <https://wiki.init.mpg.de/share/Lizenzverwaltungssoftware/xataface><br>
Xataface <http://www.xataface.com><br>

## Screenshot ##

<a href="db_export/images/overview.png" title="Ueberblick Datenbanken"><img src="db_export/images/overview.png" align="left" height="100" width="100"></a>
<br><br><br><br>

## Lizenzbedingungen ##

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.<br>
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>
See the GNU General Public License for more details.<br>
See also the license file LICENSE.txt here.
