#!/bin/bash

# create tar's from all projects
# schachi 2016-02-13

cd /var/www/html

if test $# -gt 0; then
 execute=$1
else
 echo -n "Common tar erzeugen (j/[n]) : "
 read ok
 if test -z $ok; then ok="n"; fi
 if test "$ok" = "j"; then  
  echo "create tar from xataface xataface-2.1.2/ master/ template/ favicon.ico index.html info.php"
  tar --exclude-vcs -czf db_export/mpidb_mpg_common.tar.gz xataface xataface-2.1.2/ xataface-2.1.3/ master/ template/ favicon.ico index.html info.php
 fi

 execute="mpg_*"
 echo -n "Wirklich von allen db's $execute ein tar generieren (j/[n]) : "
 read ok
 if test -z $ok; then ok="n"; fi
 if test "$ok" != "j"; then exit 0; fi 
fi

echo "Make tar for folder: $execute"

for project in $execute; do
  echo "Clear files in ${project}/templates_c und user_config"
  rm -R ${project}/templates_c/*
  rm ${project}/user_config/*
  echo "create tar from plain project ${project}"
  tar --exclude-vcs -czf db_export/mpidb_${project}.tar.gz ${project}/
  echo "create tar from project ${project} with common files"
  tar --exclude-vcs -czf db_export/mpidb_${project}_full.tar.gz xataface xataface-2.1.2/ xataface-2.1.3/ master/ template/ favicon.ico index.html info.php LICENSE.txt ${project}/
done
