<?php
/**
 * File: index.php
 * Description:
 * -------------
 *
 * This is an entry file for this Dataface Application.  To use your application
 * simply point your web browser to this file.
 */

 // disbale table views in db - sonst kommt immer Fehlermeldung in apache
define('XATAFACE_DISABLE_PROXY_VIEWS',true);

require_once '../xataface/dataface-public-api.php';
df_init(__FILE__, "../xataface")->display();
