<?php
/**
 * A delegate class for the entire application to handle custom handling of 
 * some functions such as permissions and preferences.
 */
class conf_ApplicationDelegate {
  /**
   * Returns permissions array.  This method is called every time an action is 
   * performed to make sure that the user has permission to perform the action.
   * @param record A Dataface_Record object (may be null) against which we check
   *               permissions.
   * @see Dataface_PermissionsTool
   * @see Dataface_AuthenticationTool
   */

  function getPermissions(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    // if the user is null then nobody is logged in... no access. This will force a login prompt.
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    $role = $user->val('role');
    // specially role with lower permissions as READ ONLY
    if ( strpos($role,'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    return Dataface_PermissionsTool::getRolePermissions($role);
    // Returns all of the permissions for the user's current role.
  }

  function block__custom_javascripts() {
    echo '<script src="custom.js" type="text/javascript" language="javascript"></script>';
  }

  // Sortierung erfolgt fuer das erste Mal nicht uber sort in index.php,
  // daher die Einstellung besser hier. Aber dann ist diese nicht mehr global und
  // sollte dann nicht mehr verlinkt werden
  // Nachteil: Aenderungen muessen immer wieder nachgezogen werden

  function beforeHandleRequest() {
    $app = Dataface_Application::getInstance();  
    $query =& $app->getQuery();
    $table = $query['-table'];
    // wenn relationship dann kein sort fuer table notwendig, @ verhindert error message in log
    if ( !@$query['-relationship'] ) {
      if ( !$_POST AND !@$query['-sort'] ) {
        // Sortierung aendern Common
        if ( $query['-table'] == 'list_reiter' ) $query['-sort'] = 'reiter';
        if ( $query['-table'] == 'mpi_ablage' )  $query['-sort'] = 'zeitstempel DESC';
        if ( $query['-table'] == 'sys_user' )    $query['-sort'] = 'login';
        if ( $query['-table'] == 'mpi_users' )   $query['-sort'] = 'zeitstempel DESC';
        // Sortierung aendern individuell
        if ( $query['-table'] == 'tab_table' )   $query['-sort'] = 'zeitstempel DESC';
      }
/*
    } else {
      // Sortierung relationship's - wird aber leider in section nicht beachtet, besser ueber __sql__ in relationship,
      // aber dann evtl. wieder zeitprobleme (z.B. dateiablage)
      if ( !$_POST AND @$query['-relationship'] AND !@$query['-related:sort'] ) {
        $relation = $query['-relationship'];
        //if ( $relation == 'mengenfluss' ) $query['-related:sort'] = 'zeitstempel DESC';
        //if ( $relation == 'wartung' )     $query['-related:sort'] = 'naechste';
        //if ( $relation == 'notiz' )       $query['-related:sort'] = 'zeitstempel DESC';
      }
*/
    }
  }


}
?>
