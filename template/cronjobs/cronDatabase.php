<?php

// Cronjob Skript fuer Aktionen ausserhalb der DB
// z.B. Mailbenachrichtigung, Loeschen verwaister Ablagen etc.
// schachi 2016-06-28

  // stelle sicher das dieses Skript in einem Subdir liegt, normalerweise im Ordner cronjobs, sonst gibt es kausale Problem :-(
  chdir(__DIR__);
  chdir('../');
  //print_r (realpath(__DIR__).' '.getcwd()."\n");
  if (!is_readable('conf.ini') ) trigger_error ('Error loading config file from here '.getcwd()."\n");

  $conf = array();
  $conf = parse_ini_file('conf.ini', true);
  //print_r ($conf);
  if ( !isset( $conf['_database'] ) ) trigger_error ('Error loading config file. No database specified.');
  $dbinfo =& $conf['_database'];
  if ( !is_array( $dbinfo ) || !isset($dbinfo['host']) || !isset( $dbinfo['user'] ) || !isset( $dbinfo['password'] ) || !isset( $dbinfo['name'] ) ) {
    trigger_error ('Error loading config file.  The database information was not entered correctly.');
  }
  $db = mysqli_connect($dbinfo['host'], $dbinfo['user'], $dbinfo['password'], $dbinfo['name'] );
  if ( !$db ) trigger_error ('Failed to connect to MySQL database: '.mysqli_connect_error()."\n");

  $debug = 0;  // 1 = ausgabe und keine Mail an User
  $mailto = 0; // 0 = keine Mail an Debugger
  //$mailto = 'schachi@mpi-magdeburg.mpg.de';


  // url
  $url = 'http';
  if ( isset($conf['_own']['ssl'])) {
    if ($conf['_own']['ssl'] == 1) $url = 'https';
  }
  $path = (basename(realpath('./')));
  $host = shell_exec("hostname -f | tr -d '\n'");
  $url  = $url.'://'.$host.'/'.$path;
  if ($debug) print_r($url."\n");


// Beispiele folgend:

  // loesche alle <table>__history, welche nicht erwuenscht sind
  // Xataface hat nur einen globalen Schalter ON/OFF fuer history, aber wer braucht denn alle histories?
  $sql = "SELECT reiter FROM view_reiter WHERE reiter IN (SELECT CONCAT(lst.reiter, '__history') AS table_his FROM list_reiter AS lst LEFT JOIN view_reiter AS vReit ON lst.reiter = vReit.reiter WHERE lst.history = '0' AND vReit.table_type = 'BASE TABLE' AND lst.reiter NOT LIKE '%__history') AND table_type = 'BASE TABLE';";
  $result = mysqli_query($db, $sql) OR trigger_error ('Query reiter failed: '.mysqli_error($db)."\n");
  $count = mysqli_num_rows($result);
  if ( $count >= 1 ) {
    while($row = mysqli_fetch_assoc($result)) {
      $table = $row['reiter'];
      $sql = "DROP TABLE IF EXISTS $table;";
      if ($debug) echo "$sql\n";
      mysqli_query($db, $sql) OR trigger_error ('Query history failed: '.mysqli_error($db)."\n");
    }
  }


  // loesche alle Ablagen, welche mal Verbindung hatten und nun keine Verbindung mehr haben
  $sql = "DELETE FROM mpi_ablage WHERE ablageID IN (SELECT ablageID FROM con_ablage WHERE anlID IS NULL AND gerID IS NULL AND matID IS NULL AND lieferID IS NULL AND gasID IS NULL)";
  mysqli_query($db, $sql) OR trigger_error ('Query del ablage failed: '.mysqli_error($db)."\n");
  // loesche alle Ablagen, welche nie eine Verbindung bekommen haben
  $sql = "DELETE FROM mpi_ablage WHERE ablageID NOT IN (SELECT con.ablageID FROM con_ablage AS con)";
  mysqli_query($db, $sql) OR trigger_error ('Query del connect failed: '.mysqli_error($db)."\n");


  // Script fuer automatisches senden von emails, wenn Minzahl erreicht oder unterschritten.
  // schachi 2016-07-07
  $sql = <<<EOT
  SELECT
   mat.tabID,
   mat.name,
   SUM(fluss.fluss) AS anzahl,
   mat.`min`,
   mat.einheit,
   mat.nachricht
  FROM
   mpi_material AS mat
   LEFT JOIN mpi_matMengenfluss AS fluss ON mat.tabID = fluss.tabID
  WHERE (mat.nachricht LIKE '%@%') AND (mat.`min` > 0) AND ((SELECT SUM(fluss) FROM mpi_matMengenfluss WHERE tabID = mat.tabID) <= mat.`min`)
  GROUP BY fluss.tabID
  ORDER BY name
EOT;
  $result = mysqli_query($db, $sql) OR trigger_error ('Query minzahl failed: '.mysqli_error($db)."\n");
  $count = mysqli_num_rows($result);
  if ( $count >= 1 ) {
    $table = 'mpi_material';
    $field = 'tabID';
    $base  = $dbinfo['name'];
    while($row = mysqli_fetch_array($result)) {
      $tabID = $row[$field];
      $name  = utf8_encode ($row['name']);
      $mail  = $row['nachricht'];
      $anz   = $row['anzahl'];
      $min   = $row['min'];
      $binde = $row['einheit'];
      $body  = "INFO: Mindestmenge in $table erreicht";
      $text  = "Artikel: $name\nAnzahl: $anz\nMinimum: $min\nEinheit: $binde\n";
      $link  = "Link Artikel: $url/index.php?-table=$table&-action=browse&$field=$tabID\n";
      $head  = "From: Database ".$base." <".$mail.">\n";
      $head .= "Content-Type: text/plain; charset=utf-8\n";
      $head .= "MIME-Version: 1.0\n";
      if ($debug) {
        print_r( "$mail\n$body\n${text}${link}\n$head\n" );
        if ($mailto != '0') mail( $mailto, $body, $text.$link, $head );
      } else {
        mail( $mail, $body, $text.$link, $head );
      }
    }
  }


  // Script fuer automatisches senden von emails, wenn Wartungstermin erreicht oder unterschritten.
  // schachi 2016-07-07
  function sendMail($db, $pre, $delay, $base, $table, $field, $url, $debug, $mailto) {
    $sql = <<<EOT
    SELECT
      ger.tabID,
      wart.autoID,
      wart.status,
      wart.intervall,
      wart.letzte,
      wart.naechste,
      wart.nachricht,
      ger.name,
      ger.lagerort
    FROM
      mpi_gerWartung AS wart
    LEFT JOIN
      mpi_geraete AS ger ON ger.tabID = wart.tabID
    WHERE
      (wart.naechste = ADDDATE( CURDATE(), $delay)) AND (wart.status = 1) AND (wart.nachricht LIKE '%@%')
EOT;
    $result = mysqli_query($db, $sql) OR trigger_error ('Query wartung failed: '.mysqli_error($db)."\n");
    $count = mysqli_num_rows($result);
    if ($debug) print_r ("\n$count $pre $delay\n");
    if ( $count >= 1 ) {
      while($row = mysqli_fetch_array($result)) {
        $tabID = $row[$field];
        $name  = utf8_encode ($row['name']);
        $mail  = $row['nachricht'];
        $raum  = $row['lagerort'];
        $next  = $row['naechste'];
        $last  = $row['letzte'];
        if ( $delay > 0 ) { 
          $ende = 'ist in '.$delay.' Tagen.';
        } elseif ( $delay < 0 ) {
          $ende = 'ist überschritten!';
        } else {
          $ende = 'endet heute!';
        }
        $body  = "$pre Wartungs- bzw. Prüftermin für $name $ende";
        $text  = "Gerät: $name\nRaum: $raum\nLetzter Termin: $last\nAktueller Termin: $next\n";
        $link  = "Link Wartungs- bzw. Prütermin: $url/index.php?-table=$table&-action=browse&$field=$tabID\n";
        $head  = "From: Database ".$base." <".$mail.">\n";
        $head .= "Content-Type: text/plain; charset=utf-8\n";
        $head .= "MIME-Version: 1.0\n";
        if ($debug) {
          print_r( "$mail\n$body\n${text}${link}\n$head\n" );
          if ($mailto != '0') mail( $mailto, $body, $text.$link, $head );
        } else {
          mail( $mail, $body, $text.$link, $head );
        }
      }
    }
  }

  if (!isset( $conf['_own']['notify'] )) $delay = 30; else $delay = $conf['_own']['notify'];
  $base = $dbinfo['name'];
  $table = 'mpi_gerWartung';
  $field = 'autoID';
  sendMail( $db, '[INFO]',    $delay, $base, $table, $field, $url, $debug, $mailto );
  sendMail( $db, '[TERMIN]',  '7',    $base, $table, $field, $url, $debug, $mailto );
  sendMail( $db, '[WICHTIG]', '0',    $base, $table, $field, $url, $debug, $mailto );
  sendMail( $db, '[WARNUNG]', '-7',   $base, $table, $field, $url, $debug, $mailto );


  mysqli_close($db);

?>
