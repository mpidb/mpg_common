- Dieser Ordner "template" kann als Vorlage fuer neue Datanbanken genutzt werden.
- Kopiere einfach template zu deinem neuen Datenbankverzeichnis.
  Beispiel: cp -a template <mpidb_new>
- Passe DB-Eintraege in conf.ini an Mysql-DB an
- Passe Tabelleneintraege in tables deinen Tabellen an

